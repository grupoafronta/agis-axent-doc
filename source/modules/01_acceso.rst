##############
¿Cómo Acceder?
##############


aGIS es una aplicación multiplataforma totalmente WEB, que permite el acceso mediante cualquier navegador convencional, aunque está optimizado para:

- Mozilla Firefox 66.0.5 o posterior
- Google Chrome versión 74 o posterior

Para acceder basta con ir a la URL https://agis-axent.grupoafronta.com/ .

.. figure::  images/01_acceso/login_axent.png
   :align:   center
   :width: 80%

   Ventana inicial de Login.