########################
Funcionalidades del Mapa
########################

El acceso al mapa principal (núcleo o core de la aplicación) es muy sencillo y accesible gracias al menú lateral que nos permite, desde cualquier lugar dentro de la aplicación, acceder clicando en el botón "Mapa".


.. figure::  images/03_funcionalidades/image1.png
   :align:   center
   :width: 40%


Una vez dentro, podemos comprobar que se despliegan más opciones que examinaremos más adelante.


Mapa
****

.. figure::  images/03_funcionalidades/image0.png
   :align:   center
   :width: 80%


Se permite visualizar y acceder a las distintas herramientas y utilidades que permiten interactuar con todo el contenido de la plataforma. La plataforma se compone, principalmente, de dos componentes, un Mapa Base, el cual se puede intercambiar entre los que dispone la plataforma, y un conjunto de capas que se sobreponen al Mapa Base.


Mapa base
=========
Una vez dentro del mapa existe una serie de herramientas básicas:

**1. Copiar coordenadas**

Mediante este botón que se encuentra en la parte superior derecha, podemos seleccionar un punto en el mapa y que nos diga sus coordenadas, tanto en metros como en grados (si pulsamos sobre “Latitud” nos cambiarán las unidades de medida). Simplemente, hacemos clic en un punto del mapa y nos aparecerá arriba a la derecha. Una vez hecho, clicamos en el botón "Copiar Coord" y se nos copiará en el portapapeles.

.. figure::  images/03_funcionalidades/image2.png
   :align:   center
   :width: 50%


.. figure::  images/03_funcionalidades/image3.png
   :align:   center
   :width: 50%


**2. Zoom**

Zoom nos permite acercar o alejar la vista del mapa. También está la opción de hacerlo con la rueda del ratón.

.. figure::  images/03_funcionalidades/image4.png
   :align:   center

**3. Pantalla completa**

Nos muestra la vista del mapa a pantalla completa.

.. figure::  images/03_funcionalidades/image5.png
   :align:   center


**4. Selector de nivel**

Esta herramienta nos permite cambiar de planta en el mapa, permitiendo así diferentes alturas. Simplemente, le daremos a la flecha hacia arriba para una planta superior y a la flecha hacia abajo para una planta inferior. El cuadrado central muestra la planta actual.

.. figure::  images/03_funcionalidades/image6.png
   :align:   center


**5. Selector mapa base**

La plataforma dispone de distintos Mapas Base con los que poder trabajar como referencia. Para cambiar los Mapas Base, se debe acceder al botón en forma de mapa del mundo.

.. figure::  images/03_funcionalidades/image7.png
   :align:   center

Actualmente, al cambiar de mapa base, se borran y reinician literalmente todas las capas no vectoriales, añadiendo el basemap nuevo. Esto optimiza el visionado y evita problemas.

Cuando se accede al menú, aparece una nueva ventana con todos los mapas disponibles y una previsualización de los mismos.

.. figure::  images/03_funcionalidades/image8.png
   :align:   center
   :width: 80%

La carga de estos mapas es dinámica, es decir, podremos encontrar más o menos mapas según el proyecto o fecha. Algunos de los mapas por defecto son los siguientes:

* ESRI Carreteras:
    Recurso público de ESRI que muestra la red de carreteras a nivel Global.

* Google Carreteras:
    Mapa vectorial de las carreteras, con etiquetado.

* Google Satélite Hybrid:
    Ortofoto Satélite pública de Google, con etiquetado.

* Google Satélite:
    Ortofoto Satélite pública de Google, sin etiquetado.

* Esri Satélite:
    Recurso público de ESRI que muestra ortofoto a nivel Global.

* CARTO Mapa claro:
    Recurso de CARTO que provee un mapa de carga sencilla, con líneas sencillas, muy recomendable para una visualización básica.

* IGN PNOA:
    Ortofotos de máxima actualidad del IGN.

* IGN Catastro:
    Recurso público del IGN (Instituto Geográfico Nacional) de catastros nacionales.

* IGN Catastro B/N:
    Recurso público del IGN (Instituto Geográfico Nacional) de catastros nacionales en blanco y negro.

* OSM:
    Recurso público y colaborativo de callejeros y catastros. Muy recomendado.

**6.Medir distancia y áreas**

Esta herramienta nos permite medir, tanto distancias de un punto a otro, como el perímetro o el área.

.. figure::  images/03_funcionalidades/image9.png
   :align:   center

Para medir, hacemos clic en la herramienta y seleccionamos los puntos en el mapa haciendo un clic.

.. figure::  images/03_funcionalidades/image10.png
   :align:   center
   :width: 30%

Para medir el perímetro o el área, vamos seleccionando los puntos que conforman el polígono y, cuando vayamos a cerrarlo, hacemos doble clic.

.. figure::  images/03_funcionalidades/image11.png
   :align:   center
   :width: 30%

**7. Escala**

Esta herramienta se encuentra situada en la esquina inferior izquierda y nos muestra una escala que va cambiando según la ampliación del mapa.

.. figure::  images/03_funcionalidades/image12.png
   :align:   center


Mi Ubicación
============

Esta herramienta aparece en la parte izquierda de la pantalla y solo será visible cuando estemos en el menú de Mapa.

Al clicar sobre Mi Ubicación, se colocará sobre la ubicación del usuario un marcador y se centrará el mapa sobre esa ubicación ofreciendo sus coordenadas.

.. figure::  images/03_funcionalidades/image14.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image15.png
   :align:   center
   :width: 30%


Vista de Datos
==============

Nos permite ver una tabla con datos significativos de los distintos elementos que previamente debemos seleccionar. Dicha tabla nos permite además, funcionalidades extra como son exportarla a excel, copiar una fila al portapapeles e imprimir la tabla o llevarnos directamente al lugar del mapa en el que se encuentra un elemento.

Para esto último debemos seleccionar una fila haciendo clic encima de ella y pulsar icono del ojo.

.. figure::  images/03_funcionalidades/image16.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image17.png
   :align:   center
   :width: 80%


Búsqueda en Red
===============

La pestaña de Búsqueda en Red permite localizar elementos por el campo nombre o bien, en caso de tenerlo, por el campo nombre alternativo. Esta función actúa para capas puntuales y lineales. Las capas, donde se desean hacer las búsquedas las proporcionan la herramienta mediante un menú checkbox.

.. figure::  images/03_funcionalidades/image13.png
   :align:   center
   :width: 30%

Filtros
=======

La pestaña de filtros ayuda al usuario a la hora de encontrar un objeto en específico. Se puede filtrar por:

.. figure::  images/03_funcionalidades/image18.png
   :align:   center
   :width: 30%


* Capas Dorsales

.. figure::  images/03_funcionalidades/image19.png
   :align:   center
   :width: 30%

Podemos seleccionar los distintos elementos que se encuentran en la capa Dorsales.


* Capas RS Indoor

.. figure::  images/03_funcionalidades/image20.png
   :align:   center
   :width: 30%

Podemos seleccionar los distintos elementos que se encuentran en la capa RS Indoor.


* POPs

Dividido en cuatro opciones filtrados combinables entre si --> Tipo, Titular, Estado Pop y Origen.

.. figure::  images/03_funcionalidades/image21.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image22.png
   :align:   center
   :width: 30%

Podemos seleccionar los distintos elementos que se encuentran en la capa Pops.


* Segmentos

Dividida en cuatro opciones filtrados combinables entre si --> Titular, Estado, Capacidad o Cubierta.

.. figure::  images/03_funcionalidades/image23.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image24.png
   :align:   center
   :width: 30%

Podemos seleccionar los distintos elementos que se encuentran en la capa Segmentos.


* Canalizaciones

Dividida en tres opciones filtrados combinables entre si --> Propietario, Estado y Tipo.

.. figure::  images/03_funcionalidades/image25.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image26.png
   :align:   center
   :width: 30%

Podemos seleccionar los distintos elementos que se encuentran en la capa Canalizaciones.


* RS Mobiliario

Capa filtrada por Propietario.

.. figure::  images/03_funcionalidades/image27.png
   :align:   center
   :width: 30%


Capas
=====

aGIS, como Sistema de Información Geográfica, modela los elementos  mediante un sistema de capas que permite la flexibilidad y escalabilidad del sistema.

Sobre el mapa base, se visualizarán estas capas con los distintos elementos, atendiendo a un sistema de visualización.

Las capas son los distintos elementos que se observan a la hora de visualizar la red. Las Capas pueden ser puntuales, lineales o poligonales. También hay un conjunto de capas no geométricas que complementan a las Capas geométricas.

**1. Segmentos**
----------------
    Capa geométrica y lineal que representa el cableado de red de telecomunicaciones (fibra óptica, cobre, coaxial…).

**2. Subsegmentos**
-------------------
    Capa no geométrica, heredada de segmento, que representa los circuitos físicos dentro de un determinado segmento (hilo de fibra, hilo de cobre, radioenlace…).

**3. Cocas**
------------
    Capa geométrica, y puntual, que representa las cocas existentes, una coca es aquel segmento adicional en forma de espiral, bucle o rollo que se deja antes de una conexión, de forma que resulta flexible para absorber dilataciones.

**4. Equipos**
--------------
    Capa geométrica y puntual que representa el equipamiento de la red.

**5. Subequipos**
-----------------
    Capa no geométrica, heredada de equipos, que permite representar patch pannels, bandejas, y splitters dentro de un equipo.

**6. Terminales**
-----------------
    Capa no geométrica, heredada de terminales, que permite representar cualquier terminal de patch pannel, bandeja o splitter conectable.

**7. Estructuras**
------------------
    Capa geométrica, y puntual, que representa infraestructura puntual como cámaras, arquetas, postes...

**8. Canalizaciones**
---------------------
    Capa geométrica y lineal que representa infraestructura lineal como canalización, zanja, pasos aéreos...

**9. Conductos**
----------------
    Capa no geométrica, heredada de canalización para representar conductos dentro de arquetas.

**10.Subconductos**
-------------------
    Capa no geométrica, heredada de conducto para representar subconductos dentro de conductos.

**11. POPs**
------------
    Capa geométrica y poligonal para representar emplazamientos y nodos de la red.

**12. Asociaciones**
--------------------
    Capa no geométrica que permite asociar segmentos a canalizaciones mediante el parámetro uuid.

**13. Conexiones**
------------------
    Capa no geométrica que establece enlaces 1 a 1 entre pares terminal-terminal, terminal-fibra, fibra-terminal, y que contiene toda la información de conexionado de la red.

**14. Servicios**
-------------------
    Capa no geométrica  para almacenar información de los servicios.

**15. Zonas**
-------------
    Capa geométrica y poligonal que representa las diferentes zonas disponibles en el proyecto.

**16. Actuaciones**
-------------------
    Capa no geométrica  para almacenar un log de las acciones realizadas sobre las capas.

**17. RS observaciones**
------------------------
    Capa geométrica y puntual para indicar anotaciones sobre el mapa.

**18. RS mobiliario**
---------------------
    Capa geométrica y poligonal para representar el emplazamiento donde se sitúa el equipamiento de la red.

**19. RS planta**
-----------------
    Capa geométrica y lineal para representar edificaciones.


Diseño
******

.. figure::  images/03_funcionalidades/image28.png
   :align:   center
   :width: 30%

Gestor de Modelos
=================

Es una de las herramientas principales de aGIS, ya que nos permite dibujar en el mapa los distintos elementos disponibles y asignarles los campos necesarios.

La interfaz del gestor de modelos se divide en tres partes:


**Selector de elemento:** Nos permite elegir el elemento que deseemos dibujar en el mapa.

.. figure::  images/03_funcionalidades/image29.png
   :align:   center
   :width: 40%


**Datos generales:** Formulario que dependiendo del elemento seleccionado varía sus campos para una mayor precisión.

.. figure::  images/03_funcionalidades/image31.png
   :align:   center
   :width: 35%

**Datos Específicos:** Formulario opcional que permite que realicemos un seguimiento temporal del elemento, incluso con la opción de añadir observaciones. Contiene estos campos:

    * Fechas de última modificación
    * Fecha de Instalación
    * Fecha de Alta
    * Fecha de Baja
    * Observaciones

.. figure::  images/03_funcionalidades/image30.png
   :align:   center
   :width: 35%

Una vez rellenados los campos necesarios, pulsamos en el botón Dibujar para situar el objeto en el mapa. En el caso de capas lineales o poligonales, para finalizar el trazado, debemos hacer doble clic en el último punto que realicemos.


1. Segmentos
------------

.. figure::  images/03_funcionalidades/image32.png
   :align:   center
   :width: 40%

**DATOS GENERALES:**
            **Nombre:**
                Nombre del segmento.

            **Tipo:**
                Los tipos de segmento disponibles son los siguientes:

                * Fibra G.655
                * Fibra G.652D
                * Fibra Mixto
                * Radioenlace
                * Coaxial
                * Alimentación
                * Tierra

            **Capacidad:**
                La cantidad de fibras que tendrá el segmento.

            **Situación:**
                Los siguientes tipos de situaciones indica en qué medio se encuentra el segmento.

                * Canalizado
                * Fachada
                * Interior
                * Poste
                * Zanja
                * Corrugado
                * Bandeja
                * Techo
                * Pared
                * Suelo
                * Vertical

            **Cubierta:**
                La cubierta es la parte exterior del segmento preparada para proteger el interior del mismo de los agentes externos. Las cubiertas de las mangueras, dependiendo de la aplicación o lugar de instalación, pueden ser:

                * PESP
                * KT
                * TKT
                * PKP
                * PKCP
                * PVC
                * Polietileno
                * Poliolefinas
                * Poliuretano
                * Teftel
                * Fluorados
                * Etileno Propileno
                * Polietileno Reticulado
                * Acetato Vinil
                * Silicona
                * Neopreno
                * Caucho
                * Pantalla
                * FVP
                * Sin Cubierta

            **Código de Colores:**
                Código de colores del fabricante.

            **Propietario:**
                El dueño del cableado.

            **Tipo de Red:**
                * Cesión Inicial
                * Bitubo Vacío
                * Nueva Construcción
            (Sólo aparecerá cuando el propietario sea ENAGAS)

            **Estado:**
                Indicar en qué estado se encuentra la instalación.

                * Proyectado
                * En construcción
                * Instalado

            **Modalidad:**
                Indica la modalidad en la que se encuentra el segmento.

                * Alquilado
                * Propietario
                * Derecho de Uso
                * Cesión

2. Canalizaciones
-----------------

.. figure::  images/03_funcionalidades/image33.png
   :align:   center
   :width: 40%

**DATOS GENERALES:**

            **Nombre:**
                Nombre de la canalización.

            **Tipo:**
                Indica cómo se realizará la canalización.

                * Canalización
                * Zanja
                * Paso Aéreo
                * Cruce Especial

            **Estado:**
                Indicar en qué estado se encuentra la instalación.

                * Proyectado
                * En construcción
                * Instalado

            **Propietario:**
                Dueño de la canalización

            **Modalidad:**
                * Alquilado
                * Propietario
                * Derecho de Uso
                * Cedido

            **Dimensión X**

            **Dimensión Y**

            **Anclajes:**
                Número de Anclajes en Canalización o paso Aéreo

            **Obturadores Totales:**
                Número de Obturadores Totales en la Canalización.

3. Cocas
--------

.. figure::  images/03_funcionalidades/image34.png
   :align:   center
   :width: 40%

**DATOS GENERALES:**
            **Nombre:**
                Nombre de la coca.

            **Referencia Segmento:**
                Segmento al que pertenece la COCA.

            **Longitud**
                Longitud de la canalización.


4. Equipos
----------

.. figure::  images/03_funcionalidades/image35.png
   :align:   center
   :width: 40%

**DATOS GENERALES:**

            **Nombre:**
                Nombre del equipo.

            **Estado:**

                * Proyectado
                * En construcción
                * Instalado

            **Propietario:**
                Dueño de la canalización

            **Modalidad:**
                * Alquilado
                * Propietario
                * Derecho de Uso
                * Cedido

            **Tipo:**
                * Repartidor
                * CD
                * T
                * CE
                * P2P
                * ANTENA
                * SWITCH
                * FUERZA
                * RADIO
                * SISTEMA

            **Situación:**
                * ARQUETA
                * CAMARA
                * FACHADA
                * INTERIOR
                * PEDESTAL
                * POSTEL
                * REGISTRO
                * MASTIL
                * TORRE
                * RACK
                * CASETA

            **Fabricante:**
                * COMMSCOPE

            **Modelo:**
                * FIST-GR3
                * FIST-GCO2-BC6
                * FOMS-FPS-HD-R-SIL1
                * FIST-WR2
                * ENAGAS

5. Pops
-------

.. figure::  images/03_funcionalidades/image36.png
   :align:   center
   :width: 40%

**DATOS GENERALES:**

            **Id Pop:**
                Id del Pop.

            **Nombre:**
                Nombre del Pop.

            **Tipo**

                * Posición
                * Arqueta
                * Centro de Telecomunicaciones

            **Origen**
                Origen del Pop.

            **Titular**
                Titular del Pop.

            **Código de Terceros**
                Código de identificación de terceros.

            **Dirección**
                Dirección del Pop.

            **Estado Pop:**
                * Candidato
                * No Disponible
                * Disponible con Condiciones
                * Baja
                * Disponible

            **Nombre de Terceros**
                Nombre de terceros.

6. Estructura
-------------

.. figure::  images/03_funcionalidades/image37.png
   :align:   center
   :width: 40%

**DATOS GENERALES:**

            **Nombre:**
                Nombre de la Estructura.

            **Tipo:**
                * Arqueta
                * Cámara de registro
                * Poste
                * Perforación

            **Función:**
                * Segregación
                * Línea
                * Caseta Axent
                * Caseta Cliente

            **Material:**
                * Hormigón
                * En madera
                * Fundición
                * Fibra de Vidrio

            **Propietario:**
                Nombre del propietario.

            **Modalidad:**

                * Alquilado
                * Propietario
                * Derecho de Uso
                * Cedido

            **Estado:**

                * Proyectado
                * En construcción
                * Instalado

            **Dimesión X:**
                Valor dimesión X.

            **Dimesión Y:**
                Valor dimesión Y.

            **Dimesión Z:**
                Valor dimesión Z.


7. RS: Huellas
--------------

.. figure::  images/03_funcionalidades/image38.png
   :align:   center
   :width: 40%

**DATOS GENERALES:**
            **Nombre:**
                Nombre de la Huella.

            **Tipo:**
                * Bastidor
                * E. Fuerza
                * Luminaria
                * Aire Acondicionado
                * Módulo de Sistema
                * Módulo de Radio
                * Transmisión
                * Antena
                * Torre
                * Mástil
                * Otros

            **Capacidad**
                Capacidad Huella.

            **Estado**
                * Proyectado
                * En Construcción
                * Instalado

            **Propietario**
                Propietario de la Huella.


8. RS: Obs
----------

.. figure::  images/03_funcionalidades/image39.png
   :align:   center
   :width: 40%

**DATOS GENERALES:**
            **Nombre:**
                Nombre de la observación.

            **Tipo:**
                * Certificación
                * Edificación
                * Instalación
                * Nota interna
                * Obra Civil
                * Replanteo
                * Etiqueta
                * Detalle

            **Observación**
                Descripción de la observación.


9. RS: Planta
-------------

        Capa auxiliar para dibujar el detalle de la planta del emplazamiento.



Ventanas de Información
=======================

Las ventanas de información se abrirán en la parte derecha de nuestra pantalla cuando se clica en los distintos elementos que se tengan representados en el mapa. Las ventanas de información varían según el elemento que se clica y según los servicios disponibles. En resumen, las ventanas de información desglosan de forma resumida toda la información del objeto que se haya seleccionado.


Mediante las ventanas de información, podremos terminar de configurar los distintos elementos, ver sus esquemas, ver documentos relacionados, obervaciones y modificar los datos que se introdujeron cuando se crearon.


.. figure::  images/03_funcionalidades/image40.png
   :align:   center
   :width: 40%

Como podemos observar en la imagen las interfaces están compuestas  por:

    **Nombre del elemento:**
        Es el nombre que establecemos al crear el elemento.


    **Elemento:**
        Tipo de elemento.


    **Datos Generales:**
        Contiene los datos generales de creación de un tipo de elemento en concreto.


    **Esquema:**
        Nos proporciona diferentes esquemas.


    **Datos Específicos:**
        Contiene datos concretos sobre la creación (fechas y observaciones).


    **Documentos:**
        Nos muestra los diferentes documentos asignados al elemento desde la utilidad Gestor Documental.


    **Observaciones:**
        Nos muestra las diferentes incidencias asignadas al elemento.


Además de estas pestañas, disponemos de tres botones en la parte superior derecha.


.. |image41| image:: images/03_funcionalidades/image41.png
    :width: 40

.. |image42| image:: images/03_funcionalidades/image42.png
    :width: 20

.. |image43| image:: images/03_funcionalidades/image43.png
    :width: 30

**Modificar** |image41|

    Para modificar un elemento, se debe presionar sobre el icono del lápiz y editar los datos generales o específicos. Desde esta ventana, podemos eliminar un elemento pulsando el botón eliminar.  Dependiendo del elemento, se habilitarán diferentes opciones concretas que analizaremos más adelante.


**Expandir/minimizar** |image42|

    Permite la minimización y expansión de la ventana de información.


**Cerrar** |image43|

    Mediante este botón podemos cerrar la ventana de información.


Vamos a analizar las opciones más concretas que poseen los distintos elementos:

a. Modificación del trazado
---------------------------
Los segmentos, las canalizaciones y líneas permiten la modificación de su trazado con la opción de Modificar.

Para ello, debemos estar en el modo de edición de uno de los elementos antes mencionados.


.. figure::  images/03_funcionalidades/image44.png
   :align:   center
   :width: 80%

Como vemos en la captura el trazado aparece en azul y se marcan un cuadrado en cada extremo y uno central.

Desde los extremos, podemos extender, encoger y mover la posición del punto.

.. figure::  images/03_funcionalidades/image45.png
   :align:   center
   :width: 80%

Como vemos en la imagen, hemos desplazado el trazado del segmento.

Desde el cuadrado central, podemos añadir otro vértice al trazado.

.. figure::  images/03_funcionalidades/image46.png
   :align:   center
   :width: 80%

Entre cada vértice, se creará un cuadrado intermedio para poder editar.


b. Canalizaciones
-----------------

Desde la ventana de canalizaciones, podemos añadir conductos y subconductos de una manera sencilla.

.. figure::  images/03_funcionalidades/image47.png
   :align:   center
   :width: 40%


La canalización vacía se representa de la siguiente forma:


.. figure::  images/03_funcionalidades/image48.png
   :align:   center
   :width: 30%

.. |image49| image:: images/03_funcionalidades/image49.png
    :width: 80

**i. Añadir conducto**
    Para ello, desplegamos la pestaña conductos y subconductos. En la parte de conductos, seleccionamos las opciones que deseemos y le damos al botón |image49|

    Una vez hecho esto, nos aparecerá a la derecha de Conductos COND 1, si lo seleccionamos

.. figure::  images/03_funcionalidades/image50.png
   :align:   center
   :width: 30%

   Ahí aparecerán los conductos que vayamos creando.


.. figure::  images/03_funcionalidades/image51.png
   :align:   center
   :width: 30%

   Representación de un conducto


.. |image52| image:: images/03_funcionalidades/image52.png
    :width: 80


**ii. Eliminar conducto**
    Para eliminar un conducto, tan solo debemos seleccionarlo haciendo clic en el COND que queramos eliminar y darle al botón |image52|.


.. |image53| image:: images/03_funcionalidades/image53.png
    :width: 100

**iii. Añadir subconducto**
    Para añadir un subconducto, primero, debemos seleccionar un conducto. Una vez seleccionado el COND, elegimos las opciones que deseemos y le damos al botón |image53|.


    Una vez hecho, esto nos aparecerá a la derecha de Subconductos SUBCOND 1

.. figure::  images/03_funcionalidades/image54.png
   :align:   center
   :width: 30%

   Ahí aparecerán los subconductos que vayamos creando, según el conducto preseleccionado.



.. figure::  images/03_funcionalidades/image55.png
   :align:   center
   :width: 30%

   Representación de un subconducto

.. |image56| image:: images/03_funcionalidades/image56.png
    :width: 100

**iv. Eliminar subconducto**
    Para eliminar un subconducto, tan solo debemos seleccionarlo haciendo clic en el SUBCOND que queramos eliminar y darle al botón |image56|.

**v. Esquema**
    Una vez realizados los conductos y subconductos, podemos hacer un zoom para ver el esquema de la canalización. Sólo hará falta hacer clic encima del subconducto.

.. figure::  images/03_funcionalidades/image57.png
   :align:   center
   :width: 30%

   Vista general de canalización

.. figure::  images/03_funcionalidades/image55.png
   :align:   center
   :width: 30%

   Vista ampliada de canalización


c. Equipos
----------

Desde la ventana de equipos, podemos añadir bandejas y patch pannels.

.. figure::  images/03_funcionalidades/image58.png
   :align:   center
   :width: 40%

.. figure::  images/03_funcionalidades/image59.png
   :align:   center
   :width: 40%

.. |image60| image:: images/03_funcionalidades/image60.png
    :width: 80

**i. Bandejas**
    Para ello, desplegamos la pestaña bandejas. Seleccionamos la capacidad que deseemos y le damos al botón |image60|.

    Una vez hecho esto, nos aparecerá a la derecha de Nº Bandejas: BAND 1

.. figure::  images/03_funcionalidades/image62.png
   :align:   center
   :width: 50%

   Apareceren las bandejas que vamos creando


    .. |image61| image:: images/03_funcionalidades/image61.png
       :width: 80

   Para eliminar una bandeja seleccionamos el BAND y le damos al botón |image61|

.. figure::  images/03_funcionalidades/image63.png
   :align:   center
   :width: 30%

   Esquema de la bandeja

.. |image64| image:: images/03_funcionalidades/image64.png
   :width: 80

**iii. Patchs**
    Para ello, desplegamos la pestaña patch. Seleccionamos la capacidad y el rótulo que deseemos y le damos al botón |image64|.


    Una vez hecho esto nos aparecerá a la derecha de Nº Patchs: PATCH 1

.. figure::  images/03_funcionalidades/image66.png
   :align:   center
   :width: 40%

   Apareceren los patchs que vamos creando

    .. |image65| image:: images/03_funcionalidades/image65.png
        :width: 100

    Para eliminar un patch, seleccionamos el PATCH y le damos al botón |image65|.


.. figure::  images/03_funcionalidades/image67.png
   :align:   center
   :width: 40%

   Esquema del patch pannel

d. Exportación de esquemas
--------------------------

Los esquemas de los diferentes elementos se pueden exportar en el formato de imagen png.

.. |image68| image:: images/03_funcionalidades/image68.png
    :width: 25

Simplemente, debemos hacer clic en el botón de exportar |image68|


.. figure::  images/03_funcionalidades/image69.png
   :align:   center

.. figure::  images/03_funcionalidades/image70.png
   :align:   center

e. Propagación
--------------

Esta herramienta la analizaremos más detalladamente en lógica de red.

En resumen, la propagación nos permite resaltar el camino lógico de  continuidad de una fibra, tanto en el esquema de una bandeja como en el de una fibra. Simplemente, pulsamos sobre la fibra o el terminal de uno de los esquemas.

.. figure::  images/03_funcionalidades/image71.png
   :align:   center
   :width: 80%

Como vemos en la imagen, se muestra el recorrido de la propagación sombreado en amarillo después de haber seleccionado el patch 1 y la posición 3 del segmento.


Tabla de Atributos
==================

La tabla de atributos muestra  las distintas capas del mapa

.. figure::  images/03_funcionalidades/image72.png
   :align:   center
   :width: 80%

.. |image73| image:: images/03_funcionalidades/image73.png
    :width: 25

.. |image74| image:: images/03_funcionalidades/image74.png
    :width: 25

.. |image75| image:: images/03_funcionalidades/image75.png
    :width: 25

.. |image76| image:: images/03_funcionalidades/image76.png
    :width: 25

.. |image77| image:: images/03_funcionalidades/image77.png
    :width: 25

.. |image78| image:: images/03_funcionalidades/image78.png
    :width: 25

.. |image79| image:: images/03_funcionalidades/image79.png
    :width: 25

.. |image80| image:: images/03_funcionalidades/image80.png
    :width: 25

|image73| Mostrar en el mapa el elemento seleccionado.

|image74| Borrar el elemento seleccionado.

|image75| Restaurar datos iniciales.

|image76| Guardar cambios.

|image77| Exportar a .CSV.

|image78| Exportar a .SHP.

|image79| Exportar a .KML.

|image80| Exportar a .GPKG.

Gestor de Conexión
==================

El gestor de conexiones nos permite conectar segmentos con equipos.

Algo a tener en cuenta es que para que, un equipo esté conectado a un segmento, deberemos entrar en el modo de edición del equipo y pegar la cruceta roja al segmento, como vemos en la siguiente captura:

.. figure::  images/03_funcionalidades/image81.png
   :align:   center
   :width: 80%

Una vez posicionado correctamente, le damos a guardar para confirmar los cambios.

Para usar la herramienta gestor de conexiones, hacemos clic en Gestor Conexiones, se nos abrirá la siguiente interfaz:

.. figure::  images/03_funcionalidades/image82.png
   :align:   center
   :width: 55%

Como vemos en la imagen, la casilla origen aparece marcada en amarillo, por lo que debemos seleccionar el origen de la conexion. En el ejemplo, seleccionaremos al equipo haciendo clic sobre él.

Una vez seleccionado el equipo, aparecerán las conexiones posibles que anteriormente habremos configurado en el mismo. Ahora, debemos hacer clic en la casilla Destino para que se ponga en amarillo y, posteriormente, seleccionamos el destino haciendo clic en él, sobre el mapa. Una vez hecho, se cargan sus posibles conexiones.

.. figure::  images/03_funcionalidades/image83.png
   :align:   center
   :width: 40%

   Origen seleccionado


.. figure::  images/03_funcionalidades/image84.png
   :align:   center
   :width: 40%

   Destino seleccionado


Para realizar la conexión, debemos seleccionar las conexiones que queremos que se unan, para seleccionar varias al mismo tiempo, dejaremos pulsada la tecla CTRL mientras hacemos clic en ellas.

Hecho esto le daremos al botón verde para unirlas o al botón rojo si queremos eliminarlas.

.. figure::  images/03_funcionalidades/image85.png
   :align:   center
   :width: 40%

   Antes de la conexión


.. figure::  images/03_funcionalidades/image86.png
   :align:   center
   :width: 45%

   Después de la conexión

Cuando realizamos conexiones, éstas se ven reflejadas en las ventanas de información de los elementos implicados en forma de diagramas dentro del Esquema.

.. figure::  images/03_funcionalidades/image87.png
   :align:   center
   :width: 40%

   Esquema de equipo


.. figure::  images/03_funcionalidades/image88.png
   :align:   center
   :width: 50%

   Esquema de segmento

Con lo que hemos realizado tan solo hemos conectado uno de los extremos del segmento, por lo tanto, debemos realizar el mismo proceso desde el otro extremo. Digamos que cuando seleccionamos un elemento desde la casilla de ORIGEN, estamos seleccionando un extremo, y si seleccionamos el mismo elemento desde la casilla DESTINO, estamos seleccionando el otro extremo. Podemos verlo más sencillo con el siguiente esquema:

.. figure::  images/03_funcionalidades/image89.png
   :align:   center
   :width: 70%

En el dibujo, se muestran el origen (morado) y el destino (naranja) de los diferentes elementos. El rectángulo verde, nos muestra lo que hemos configurado. Para llegar al equipo Y, debemos seleccionar en la casilla ORIGEN el tramo X y en la casilla DESTINO el equipo Y, realizamos las conexiones que deseemos y ya tendríamos conectado el equipo X al equipo Y

Si vamos otra vez al esquema del segmento, encontramos que ya se encuentra conectado por los dos extremos.

.. figure::  images/03_funcionalidades/image90.png
   :align:   center
   :width: 60%


Gestor de Disponibilidad
========================

Esta herramienta nos permite modificar la disponibilidad y la modalidad de las fibras de un segmento.

Cuando usamos esta herramienta, se nos abre una ventana en la derecha que nos pide que seleccionemos un segmento del mapa.

.. figure::  images/03_funcionalidades/image92.png
   :align:   center
   :width: 60%

Una vez seleccionado el segmento, se cargarán sus fibras. Según el botón que pulsemos, aparecerá información de disponibilidad o de modalidad.

Si pulsamos en disponibilidad, vemos  en verde los que están disponibles y en rojo los que no están disponibles. Para cambiar la disponibilidad de una o varias fibras, tan solo debemos seleccionarlas y pulsar en la opción que deseemos.

.. figure::  images/03_funcionalidades/image93.png
   :align:   center
   :width: 60%

Si pulsamos en modalidad, ocurre lo mismo que en disponibilidad, pero nos muestra las distintas modalidades.

.. figure::  images/03_funcionalidades/image94.png
   :align:   center
   :width: 60%


Gestor de Asociaciones
======================

Esta herramienta nos permite atribuir segmentos a canalizaciones.

Para ello, abrimos la herramienta y hacemos clic primero en el segmento y luego en la canalización. Ahora hacemos clic en el conducto por el que queremos que pase nuestro segmento y le damos al botón verde para aplicar o al botón rojo para eliminar.

.. figure::  images/03_funcionalidades/image91.png
   :align:   center
   :width: 50%

Esta información se puede comprobar de nuevo yendo al Esquema de la canalización, donde al igual que en el gestor de asociaciones aparecerá en blanco por donde pase un segmento.

Herramientas Avanzadas
======================
Cortar segmentos
----------------

Para cortar un segmento debemos usar esta herramienta. Simplemente, pulsamos el botón y seleccionamos, como podemos ver en la captura, dos puntos: el primero con un clic y el segundo con doble clic.

.. figure::  images/03_funcionalidades/image95.png
   :align:   center
   :width: 80%

.. figure::  images/03_funcionalidades/image96.png
   :align:   center
   :width: 80%

.. figure::  images/03_funcionalidades/image97.png
   :align:   center
   :width: 80%

Ahora tenemos dos segmentos que se llaman igual y tienen las mismas características, sin embargo, las conexiones permanecen en los extremos por los que vienen. Esto se ve mejor con un ejemplo.

En la captura anterior, vemos que el segmento tiene conexión con un equipo y éste está conectado de la siguiente manera al segmento.

.. figure::  images/03_funcionalidades/image98.png
   :align:   center
   :width: 80%




Una vez lo hemos cortado, la parte derecha del segmento permanecerá como en la captura. En el caso en que estuviera ocupado también por la izquierda, pasarían a estar en no conectado.

El lado izquierdo del segmento, como no tiene nada conectado por la derecha, aparecerá de la siguiente manera.

.. figure::  images/03_funcionalidades/image99.png
   :align:   center
   :width: 80%

Geoimagen
---------

Esta herramienta nos permite subir una imagen, georeferenciarla y modificar su aspecto para que encaje en el lugar deseado.

Para ello, tan solo debemos seleccionar la imagen, darle a guardar y ubicar, seleccionamos el rectángulo que queremos que ocupe de forma inicial y, por último, podemos deformarla un poco para que encaje correctamente.

.. figure::  images/03_funcionalidades/image100.png
   :align:   center
   :width: 70%

.. figure::  images/03_funcionalidades/image101.png
   :align:   center
   :width: 70%


Snapping
========

El snapping es una herramienta que nos permite pegar unos elementos a otros. Esta herramienta consume muchos recursos por lo que se recomienda tenerla desactivada a excepción de que vayamos a utilizarla.

Su uso es muy sencillo, tan solo debemos seleccionar la capa o las capas a las que queremos que se pegue.

.. figure::  images/03_funcionalidades/image102.png
   :align:   center
   :width: 80%


Editor de Estilos CSS
=====================

Esta herramienta nos permite la personalización de elementos como: segmentos, equipos, canalizaciones e infraestructuras pudiendo cambiar desde colores hasta iconos y asignarle condiciones.

La configuración se realiza mediante un sistema de reglas CSS y, posteriormente, mediante un sistema de condicionales en caso de que quiera darse ese estilo a un conjunto definido de elementos. Dichas reglas se guardan en el proyecto.

En caso de querer añadir estilos, tendremos que añadir toda la casuística necesaria ya que el estilo por defecto se desactiva totalmente.

Cada uno de los elementos tiene una pestaña diferente pero la interfaz es prácticamente la misma por lo que vamos a comprobar su funcionamiento en la pestaña Equipos.

Al entrar, se nos muestra una previsualización del mapa, una tabla con las distintas personalizaciones y un botón de Nueva Regla.

.. figure::  images/03_funcionalidades/image106.png
   :align:   center
   :width: 80%

Para añadir una personalización nueva le damos al botón Nueva Regla y rellenamos el formulario que aparece como deseemos.

Se puede modificar el símbolo, el tamaño del símbolo,  la opacidad del símbolo, color de relleno, opacidad de relleno, color de borde, ancho de borde y opacidad de borde:

.. figure::  images/03_funcionalidades/image107.png
   :align:   center
   :width: 80%

   Configuración de CSS

.. figure::  images/03_funcionalidades/image108.png
   :align:   center
   :width: 80%

   Editor CSS

.. |image109| image:: images/03_funcionalidades/image109.png
    :width: 25

Para que funcione debemos asignarle una condición pulsando en |image109|

.. figure::  images/03_funcionalidades/image104.png
   :align:   center
   :width: 80%

La  condición depende del elemento al que estemos asignando, en el ejemplo lo que hemos configurado es que los equipos que tengan como situación FACHADA se les aplique el estilo (en este caso sería el número '2' debido a la integridad de datos).

.. figure::  images/03_funcionalidades/image105.png
   :align:   center
   :width: 80%

.. |image110| image:: images/03_funcionalidades/image110.png
    :width: 90

Se pueden asignar varias condiciones a un mismo estilo. Para volver usar el estilo por defecto debemos eliminar los estilos personalizados o editarlos con el botones |image110|


Opciones
========

Propietarios
------------

Con esta opción podemos añadir propietarios para los servicios. Para ello, clicamos en el botón "añadir" y rellenamos el formulario.

.. figure::  images/03_funcionalidades/image114.png
   :align:   center
   :width: 80%


EQ: Fabricantes
---------------

Con esta opción podemos añadir fabricantes para los equipos. Para ello, clicamos en el botón "nuevo fabricante" y rellenamos el formulario.

.. figure::  images/03_funcionalidades/image111.png
   :align:   center
   :width: 80%


EQ: Modelos
-----------

Con esta opción podemos añadir modelos para los equipos. Para ello, clicamos en el botón "nuevo modelo" y rellenamos el formulario.

.. figure::  images/03_funcionalidades/image112.png
   :align:   center
   :width: 80%

SG: Código de color
-------------------

Esta opción nos permite crear un código de colores de un fabricante.

Para crear un nuevo cable le damos a "Nuevo Modelo", esto permite crear tantos modelos como necesitemos. Para crear un código de colores, hay que tener claro una serie de campos:

.. figure::  images/03_funcionalidades/image115.png
   :align:   center
   :width: 80%

* NOMBRE DEL FABRICANTE:
    Identificador que, posteriormente, asignaremos al cable para la asignación de su código.
* FIBRAS POR TUBO:
    Módulo del cable, es decir, fibras por tubo, esto asignará tantos colores como fibras por tubo, que serán los mismos para cada conjunto de fibras por tubo.
* MAX TUBO:
    Número máximo de tubos que admite nuestro código de colores. Esto asignará tantos colores como tubos máximos tengamos en el cable.
* MODO 2 Colores:
    Permite que tanto tubos como fibras, admitan varios colores.

.. |image116| image:: images/03_funcionalidades/image116.png
    :width: 25

Una vez completamos los datos le damos al botón de editar (|image116|) para configurar el código de colores.

La configuración de colores es sencilla, en columna acción, tan solo hay que pulsar el botón de editar y entramos en el panel de colores.

.. figure::  images/03_funcionalidades/image117.png
   :align:   center
   :width: 80%

Por defecto, el valor es NULL, aunque aparezca en blanco, por lo que se  asigna colores pulsando sobre el círculo de la columna COLOR tanto para fibras como para tubos.

El icono de la brocha, sirve para copiar y pegar un color.
Brocha - Círculo a Copiar - Círculo a Pegar

Una vez asignados todos los colores, se puede proceder a asignarlos a un cable.

.. figure::  images/03_funcionalidades/image118.png
   :align:   center
   :width: 80%

Para asignar un color a un cable, se debe de seleccionar cable en cuestión, entrar al modo edición, y en el campo de Fabricante y Código de Colores, se introducirá el nombre del Fabricante y del Código de Color que queramos asignar respectivamente. Por ejemplo, Default.

.. figure::  images/03_funcionalidades/image119.png
   :align:   center
   :width: 80%

Al dar a Guardar, y acceder a su infowindows o ventana de información, ya se verá el código de colores, según lo que se haya asignado.

.. figure::  images/03_funcionalidades/image120.png
   :align:   center

La columna de la izquierda representa el color del tubo, y la columna de la derecha el color de la fibra.

POP: Titular
------------

Con esta opción podemos añadir titulares para los pop. Para ello, clicamos en el botón "nuevo titular" y rellenamos el formulario.

.. figure::  images/03_funcionalidades/image113.png
   :align:   center
   :width: 80%