##########
Utilidades
##########

En este menú, encontramos una serie de herramientas que complementan las utilidades de aGIS ya descritas en capitulos anteriores.

.. figure::  images//05_utilidades/image0.png
   :align:   center
   :width: 30%


Gestor de Exportación
*********************

Esta herramienta permite la exportación por selección, para ello, vamos a Utilidades -> Exportación

El display que se muestra en pantalla es sencillo y de fácil comprensión. Se muestra una zona superior en la que se muestra un mapa. En la parte inferior, se encuentra la zona donde elegimos los datos a exportar.

Para exportar los datos, se debe seleccionar una capa y el formato en el que se quiere exportar y, por último, pulsar sobre NIVEL si queremos que solo se exporte el nivel seleccionado o sobre GENERAL para exportar todos los niveles.

.. figure::  images/05_utilidades/image1.png
   :align:   center
   :width: 80%

Si deseamos exportar solo una zona del mapa, se puede seleccionar con la herramienta de polígonos, la sección deseada:

.. figure::  images/05_utilidades/image2.png
   :align:   center
   :width: 80%

Posteriormente, elegimos la capa, el formato, y si queremos que sea en el NIVEL o GENERAL.


Inventario
**********

Esta herramienta permite visualizar los equipos de red después de pulsar sobre “Inventario”. Los datos que devuelve están divididos por capas y son independientes. Estos datos los podemos seleccionar como comprobamos en la exportación por polígono, por nivel o en general.

.. figure::  images/05_utilidades/image3.png
   :align:   center
   :width: 80%

.. figure::  images/05_utilidades/image4.png
   :align:   center
   :width: 80%


Sites: Gestión
**************

Es una herramienta que nos permite gestionar los emplazamientos de radio.

Para ello, debemos cumplimentar los datos del formulario como deseemos y asignarlo a un pop.

.. figure::  images/05_utilidades/image5.png
   :align:   center
   :width: 80%

.. figure::  images/05_utilidades/image6.png
   :align:   center
   :width: 80%

Después, en el modo edición podemos rellenar el resto de campos.

.. figure::  images/05_utilidades/image7.png
   :align:   center
   :width: 80%

.. figure::  images/05_utilidades/image8.png
   :align:   center
   :width: 80%

.. figure::  images/05_utilidades/image9.png
   :align:   center
   :width: 80%

Sites: Inventario
*****************

Esta herramienta permite visualizar los equipos de red (RS) después de pulsar sobre “Sites: Inventario”. Los datos que devuelve están divididos por capas y son independientes. Estos datos los podemos seleccionar como comprobamos en la exportación por polígono, por nivel o en general.

.. figure::  images/05_utilidades/image10.png
   :align:   center
   :width: 80%

.. figure::  images/05_utilidades/image11.png
   :align:   center
   :width: 80%

.. figure::  images/05_utilidades/image12.png
   :align:   center
   :width: 80%

Gestor Documental
*****************

Con esta herramienta, podemos subir archivos de distinta índole a la plataforma, permitiendo exportar los ya existentes en formato CSV, XLS, PDF o incluso imprimirlo. A la hora de crear un nuevo documento, debemos darle al botón añadir y rellenar un formulario que se compone de:

**Nombre:** Nombre que se le quiere poner al documento en aGIS.

**Capa:** El documento en cuestión.

**Asignar Elemento:** Elemento a asignar.

**Archivo:** El documento en cuestión.

.. figure::  images/05_utilidades/image13.png
   :align:   center
   :width: 80%

.. |image15| image:: images/05_utilidades/image15.png
       :width: 25

.. |image16| image:: images/05_utilidades/image16.png
       :width: 20

Una vez guardamos, tenemos varias opciones: ver el archivo, pulsando sobre Ver, asignarlo a un objeto, con |image15| o borrarlo con |image16|.

.. figure::  images/05_utilidades/image14.png
   :align:   center
   :width: 80%

En el caso en el que lo vayamos a asignar de forma masiva, nos aparecerá una interfaz donde elegiremos primero la capa y luego elegiremos el o los objetos a los que queramos asignarle dicha documentación, moviéndolos de la columna de la izquierda (NON-SELECTED) a la columna de la derecha (SELECTED). Por último, le daremos al botón asignar.

.. figure::  images/05_utilidades/image17.png
   :align:   center
   :width: 80%


Una vez hecho esto, si vamos al elemento en el mapa y le hacemos clic, en la pestaña gestor documental aparece el documento que hemos añadido, permitiendo la opción de verlo.

.. figure::  images/05_utilidades/image18.png
   :align:   center
   :width: 80%


Gestor de Trabajos
******************

En el gestor de trabajo, se pueden observar y administrar los distintos trabajos que se han de realizar sobre la red. Para crear un nuevo trabajo, se ha de pulsar sobre la opción "Nuevo Trabajo" y rellenar el formulario que se solicita:

**Nombre:**
    Nombre que se le desea poner al trabajo.

**Descripción:**
    En caso de desear introducir alguna descripción u observación al trabajo.

**Estado:**
    Situación en la que se encuentra el trabajo.

**Capa:**
    Capa en la cual se encuentra el elemento que queremos seleccionar.

**Elemento:**
    Elemento en cuestión sobre el que ha de realizarse el trabajo.

**Activar notificaciones:**
    De manera predeterminada, el que genera el trabajo recibe notificaciones, pero adicionalmente, si marcamos esta casilla y rellenamos con otro correo, también éste recibirá las notificaciones.


Para guardar el trabajo, se ha de hacer clic sobre guardar.

.. figure::  images/05_utilidades/image19.png
   :align:   center
   :width: 80%

   Formulario Trabajos

.. figure::  images/05_utilidades/image20.png
   :align:   center
   :width: 80%

   Tabla Trabajos


Como vemos en la segunda captura, al guardar nos aparece una tabla donde se muestran todos los trabajos con información relevante. Si hacemos clic en alguno de los  que aparece en la última columna de la fila podremos:

.. |image22| image:: images/05_utilidades/image22.png
       :width: 25
.. |image23| image:: images/05_utilidades/image23.png
       :width: 25

**Visualizar** |image22|
    Nos muestra tres columnas en las que se encuentrann de izquierda a derecha: Detalles, Resolución y Documentos. Las dos últimas columnas nos permiten añadir tanto la resolución al trabajo en modo de texto como asignar documentación sobre la misma.

.. figure::  images/05_utilidades/image21.png
   :align:   center
   :width: 80%

**Editar** |image23|
    Permite editar los datos del trabajo.

**Eliminar** |image16|
    Permite borrar el trabajo.


Gestor de Incidencias
*********************

El gestor de incidencias permite administrar las incidencias relacionadas con la red, exportarlas a diferentes formatos o imprimirlas. Para crear una nueva incidencia, hay que hacer clic sobre "Nueva Incidencia" y rellenar el formulario que se solicita.

**Nombre:**
    Nombre que se le desea poner a la incidencia.

**Descripción:**
    En caso de desear introducir alguna descripción u observación a la incidencia.

**Estado:**
    Situación en la que se encuentra la incidencia.

**Capa:**
    Capa en la cual se encuentra el elemento que queremos seleccionar.

**Elemento:**
    Elemento en cuestión sobre el que está la incidencia.

**Activar notificaciones:**
    De manera predeterminada, el que genera la incidencia recibe notificaciones, pero adicionalmente, si marcamos esta casilla y rellenamos con otro correo, también éste recibirá las notificaciones.


Para guardar la incidencia, se ha de hacer clic sobre "guardar".


.. figure::  images/05_utilidades/image24.png
   :align:   center
   :width: 80%

   Formulario Incidencias

.. figure::  images/05_utilidades/image25.png
   :align:   center
   :width: 80%

   Tabla Incidencias

Como vemos en la segunda captura, al guardar nos aparece una tabla donde se muestran todas las incidencias con información relevante. Si hacemos clic en la última columna de la fila "Acción" podremos:

Ver |image22|
    Nos muestra tres columnas en las que se encuentra de izquierda a derecha: Detalles, datos sobre la incidencia, Resolución y Documentos. Las dos últimas columnas nos permiten añadir tanto la resolución a la incidencia en modo de texto como asignar documentación sobre la misma.

.. figure::  images/05_utilidades/image26.png
   :align:   center
   :width: 80%

**Editar** |image23|
    Permite editar los datos de la incidencia.

**Eliminar** |image16|
    Permite borrar la incidencia.