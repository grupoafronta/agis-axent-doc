#################
Servicios Lógicos
#################

Esta pestaña sirve para crear servicios e inyectarlos por los puertos. Disponemos de cuatro herramientas: Gestionar Servicios, Gestionar Coubicación, Gestionar Canales Ópticos y Gestionar Capacidad. Para crear un servicio e inyectarlo, debemos primero crear el servicio y asignarle un puerto (Gestionar Servicios), después debemos elegir el canal óptico por el que deseemos que vaya el servicio (Gestionar Canales Ópticos) y por último, si el servicio es de capacidad, podemos terminar de configurarlo (Gestionar Capacidad).

Además podemos crear o modificar usuarios, contratos y equipos terminal desde la pestaña llamada opciones y buscar servicios lógicos a traves de la pestaña buscar circuitos.


Buscador de Servicios
*********************

El buscador de servicios se encuentra activo con la visualización del mapa base. Aparece en el menú lateral dentro de Servicios Lógicos.

.. figure::  images/04_servicios/image1.png
   :align:   center
   :width: 40%

Una vez clicado en el se abre una ventana en el mapa en la cual escribimos el nombre del servicio a buscar. La búsqueda se puede realizar tanto por Fibra Oscura, a través de su ID o Nº de contrato, como por capacidad por su ID.

.. figure::  images/04_servicios/image2.png
   :align:   center
   :width: 50%

.. figure::  images/04_servicios/image3.png
   :align:   center
   :width: 80%

Una vez realizada la búsqueda se iluminará el servico y a su vez se abrian tres ventanas de información, donde se mostrarán la información básica del servicio, los servicios asociados, la coubicación, los POPs intermedios y los circuitos de servicio.

.. figure::  images/04_servicios/image4.png
   :align:   center
   :width: 80%

Gestión de Servicios
********************

Esta herramienta nos permite añadir, editar o borrar un circuito.

Como vemos en la interfaz, tenemos el botón para añadir circuitos, una tabla que representa los distintos circuitos y nos permite mediante botones en la columna acción: ver (muestra en el mapa la localización del servicio), editar o eliminar. Ésta tabla se puede exportar a los distintos formatos como se puede apreciar en la imagen.

Los servicios apareceran en dos tablas diferenciadas por el estado del servicio (Operativo o Reservado).

.. figure::  images/04_servicios/image5.png
   :align:   center
   :width: 80%

En el formulario, para añadir un nuevo circuito, tenemos los siguientes campos:

    - Siete campos generales referentes a ID Servicio, Número de Contrato, Nombre, Tipo de Servicio, Usuario, Estado Servicio y Fecha de Caducidad.
    - Dos para FO: Pares Contratados y Fibras Contratadas.
    - Cinco para Capacidad: Ancho de Banda O-D(UL), Ancho de Banda D-O(DW), Tipo de Banda, Canal y Tecnología.
    - Otros: Aplicación, Equipo Terminal.

Entre los tipos de servicio encontramos fibra oscura y capacidad.

.. figure::  images/04_servicios/image7.png
   :align:   center
   :width: 80%

.. |image6| image:: images/04_servicios/image6.png
       :width: 25

Una vez creado, si le damos al botón |image6|, podemos editar los campos del formulario anterior, ver o añadir los puertos inyectados de los patch o de las bandejas, ver o añadir POPs intermedios y ver o añadir Servicios Asociados.

.. figure::  images/04_servicios/image8.png
   :align:   center
   :width: 80%


Para añadir un POP intermedio podemos darle al botón "Añadir POPs intermedios" y seleccionamos el POP.

.. figure::  images/04_servicios/image9.png
   :align:   center
   :width: 80%


Para añadir un Servicio Asociado clicamos en el botón "Añadir Servicio Asociado" y seleccionamos dicho servicio.

.. figure::  images/04_servicios/image10.png
   :align:   center
   :width: 80%


Para añadir un puerto le damos a "Añadir Puerto" y rellenamos el formulario.

.. figure::  images/04_servicios/image11.png
   :align:   center
   :width: 80%

Como vemos en la captura,  en la parte izquierda tenemos los campos: nombre, conector (ST, LC, SC, APC), destino: equipo y destino: puerto. En la parte derecha se muestra información de los puertos asignables.

.. figure::  images/04_servicios/image12.png
   :align:   center
   :width: 50%


Esta parte se encuentra justo debajo de la anterior y nos permite como vemos en la siguiente captura: desasignar (4) puertos inyectados, seleccionar un tipo de equipo (podemos elegir entre repartidores, cajas de empalme y torpedos), seleccionar un equipo (1) (haciendo clic sobre él), seleccionar un subequipo (2) y por último seleccionar un puerto (3).

En  conclusión, tenemos que realizar los pasos del 1 al 3 para inyectar un servicio en un puerto.

.. figure::  images/04_servicios/image13.png
   :align:   center
   :width: 80%

Una vez tenemos inyectado el servicio, podemos comprobar su propagación.


Propagación
===========

La propagación de un servicio es ver, de forma gráfica, desde donde hasta donde va un servicio, es decir, ver el equipo origen y el equipo destino. aGIS es una herramienta muy poderosa en la cual se puede ver la propagación de los servicios de forma muy fácil e intuitiva. Para ver la propagación de un servicio, se debe colocar en el mapa sobre el equipo origen del servicio y hacer clic sobre él. Al realizar el paso previo, aparecerá una ventana con información en la cual se pueden ver los Patch Pannel del equipo:

.. figure::  images/04_servicios/image16.png
   :align:   center
   :width: 50%

Los patch pannels ahora son inteligentes, a pesar de tener dos extremos, solo se representa el extremo disponible al que no se ha conectado cable, y todo esto de forma automática y transparente para el usuario.

Se han optimizado todos los procesos de consultas y velocidad, para todos los algoritmos de propagación y representación de orígenes y destinos.

Como se observa en la imagen anterior, el dispositivo para el ejemplo posee un Patch Pannel. Se debe elegir el Patch Pannel origen de la fibra que posea el servicio, y acceder a él haciendo clic sobre él. Aparecerá un nuevo menú sobre el que se pulsará “Ver Servicios Patch” para indicar que fibras poseen servicios activos.

.. figure::  images/04_servicios/image14.png
   :align:   center
   :width: 50%


Ahora las fibras con servicios activos se representarán en color verde, las fibras activas pero sin servicios se representan en color azul, y las fibras no activas se representan en color rojo.

Para ver el trayecto de un servicio, solo se debe clicar sobre la fibra con el servicio activo y automáticamente esta fibra se resaltará en el mapa para facilitar su visualización.

.. figure::  images/04_servicios/image15.png
   :align:   center
   :width: 80%


Canales Coubicación
*******************

Esta herramienta nos permite asignar circuitos a los Servicios de Coubicación.

Como vemos en la interfaz, tenemos el botón para añadir Servicios de Coubicación, una tabla que representa los distintos Servicios de Coubicación y nos permite, mediante botones, en la columna acción: editar o eliminar. Ésta tabla se puede exportar a los distintos formatos como se puede apreciar en la imagen.

.. figure::  images/04_servicios/image17.png
   :align:   center
   :width: 80%

En el formulario, debemos rellenar los campos nombre, cliente, elegir Servicio de Fibra y elegir Huella Selector (al elegir este campo se rellenan automaticamente los campo de Huella Espacial y Huella).

.. figure::  images/04_servicios/image18.png
   :align:   center
   :width: 80%


Canales Ópticos
***************

Esta herramienta nos permite asignar circuitos a los canales ópticos de las fibras.

Como vemos en la interfaz, tenemos el botón para añadir canales ópticos, una tabla que representa los distintos canales y nos permite, mediante botones, en la columna acción: editar o eliminar. Ésta tabla se puede exportar a los distintos formatos como se puede apreciar en la imagen.

.. figure::  images/04_servicios/image19.png
   :align:   center
   :width: 80%

En el formulario, debemos seleccionar el circuito, añadirle un nombre, elegir su capacidad y elegir el canal.

.. figure::  images/04_servicios/image20.png
   :align:   center
   :width: 80%

Gestión de Capacidad
********************

Esta herramienta nos permite asignar servicios de capacidad a los canales ópticos de las fibras.

Como vemos en la interfaz, tenemos el botón para añadir el servicio de capacidad, una tabla que representa los distintos servicios y nos permite mediante botones en la columna acción: editar o eliminar. Ésta tabla se puede exportar a los distintos formatos como se puede apreciar en la imagen.

.. figure::  images/04_servicios/image21.png
   :align:   center
   :width: 80%

En el formulario, debemos seleccionar el canal óptico, añadirle un id de servicio, elegir su capacidad y opcionalmente podemos añadir origen y destino.

.. figure::  images/04_servicios/image22.png
   :align:   center
   :width: 80%


Opciones
********

Usuarios
========

En la pestaña de Usuarios, se indican todos los Usuarios que usan los servicios especificados.

.. figure::  images/04_servicios/image23.png
   :align:   center
   :width: 80%

Para añadir nuevos usuarios, se ha de pulsar sobre el botón “Añadir” y rellenar los datos que se solicitan (Nombre, Razón Social, CIF, Dirección, email y Teléfono).

.. figure::  images/04_servicios/image24.png
   :align:   center
   :width: 80%

   Formulario usuarios

Los usuarios registrados no tienen porque tener servicios asignados.

Contratos
=========

Al entrar dentro de Contratos, se pueden editar los contratos ya creados o bien crear nuevos contratos.

.. figure::  images/04_servicios/image25.png
   :align:   center
   :width: 80%

Para añadir nuevos contratos se ha de pulsar sobre el botón “Añadir” y rellenar los datos que se solicitan (Referencia, Nombre, Proveedor y Usuario).

.. figure::  images/04_servicios/image26.png
   :align:   center
   :width: 80%

   Formulario contratos

Equipo Terminal
===============

Al entrar dentro de Equipo Terminal, se pueden editar los equipos ya creados o bien crear nuevos equipos.

.. figure::  images/04_servicios/image27.png
   :align:   center
   :width: 80%

Para añadir nuevos equipos terminal, se ha de pulsar sobre el botón “Añadir” y rellenar los datos que se solicitan (Nombre, Propietario, Código, Fecha de Alta y Fecha de Baja).

.. figure::  images/04_servicios/image28.png
   :align:   center
   :width: 80%

   Formulario equipo terminal