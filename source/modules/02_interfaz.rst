#################################
Interfaz General de la Aplicación
#################################


Una vez logueados, aGIS presenta una interfaz minimalista y sencilla, con un look&feel moderno, dinámico y optimizado para realizar las distintas tareas de forma sencilla. Se ha de tener en cuenta que la interfaz puede sufrir pequeñas variaciones entre las distintas versiones de navegadores compatibles.

.. figure::  images/02_interfaz/image1.png
   :align:  center
   :width: 80%

   Mapa aGIS

Secciones de la Interfaz
************************

En la interfaz principal, se puede apreciar 3 secciones bien diferenciadas: en la parte superior, atravesando la ventana, el Header; a la izquierda, el menú; y en el resto de la interfaz, el panel de Interacción que, por defecto, muestra el Dashboard.

.. figure::  images/02_interfaz/image2.png
   :align:  center
   :width: 80%

   Dashboard aGIS

Header
======

En la sección del Header, a la derecha,  se encuentra el botón para seleccionar un proyecto asignado, y a la izquierda, el botón de minimizado del menú lateral. Por último, encontramos el botón de desconexión (“Log Out”).

.. figure::  images/02_interfaz/image3.png
   :align:  center
   :width: 80%

Desde la vista del mapa aparece en el header un buscador de direcciones, que nos proporciona la utilidad de encontrar una ubicación concreta del mapa usando las coordenadas (en grados decimales. Ej:40.437368, -3.709672) o una dirección.

.. figure::  images/02_interfaz/image4.png
   :align:  center
   :width: 80%

   Coordenadas en grados decimales

.. figure::  images/02_interfaz/image5.png
   :align:  center
   :width: 80%

   Dirección

Menú
====

En la sección del menú, encontramos los distintos apartados en los cuales se divide la plataforma, estos apartados serán explicados en profundidad posteriormente. Además, dichos apartados aparecerán o desaparecerán según el grado de permisos de los que se disponga, así, como del proyecto que esté activo. Por ejemplo, el mapa mostrará las herramientas de Edición, mientras que el Dashboard no.

.. figure::  images/02_interfaz/image6.png
   :align:   center
   :width: 30%


Dashboard
=========
En el Dashboard, se encuentra toda la información centralizada. Es la pantalla principal cuando entramos a la sesión.

.. figure::  images/02_interfaz/image7.png
   :align:   center
   :width: 80%

Como se observa en la captura, el panel es muy conciso y claro. Podemos apreciar tres columnas.

En la primera se encuentra el nombre del proyecto seleccionado.

En la columna central, se encuentra la vista rápida de notificaciones. Si hacemos clic podremos ver en detalle cada notificación.

La última columna se divide en dos ventanas, la primera nos da información genérica de Axent (dirección, dirección web, correo electrónico, teléfono de contacto...). La segunda ventana da acceso a este manual.

.. figure::  images/02_interfaz/image8.png
   :align:   center
   :width: 30%


Perfil
======

De forma suplementaria, pulsando donde aparece nuestro alias y la organización, se desplegará un menú donde apareceran los apartados del perfil.

.. figure::  images/02_interfaz/image9.png
   :align:   center
   :width: 30%

   Desplegable de perfil

.. figure::  images/02_interfaz/image10.png
   :align:  center
   :width: 80%

   Menú del perfil


Una vez dentro de perfil se pueden administrar todos los datos de nuestro usuario, o toda la información y gestión correspondiente a la área, los proyectos y los permisos. Se encuentra dividida en cinco pestañas: Organización, Gestión de usuarios, Gestión de permisos, Consultar accesos y Tokens.

Cada pestaña pertenece a un apartado del perfil, el cual mostrará los datos y configuraciones disponibles.


Organización
------------

Datos relativos a la organización o compañía de la cuenta. Modificable por el administrador.

- Nombre: Nombre de la organización.
- E-Mail: Mail de Notificaciones de la organización.
- Teléfono: Teléfono de contacto de la organización.
- Avatar: Logo usado por la organización.

.. figure::  images/02_interfaz/image11.png
   :align:   center
   :width: 80%


Gestión de usuarios
-------------------
Muestra la información de usuarios de la organización (nombre, alias, mail, teléfono y tipo de usuario). Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image12.png
   :align:   center
   :width: 80%


Gestión de permisos
-------------------

Permite a los administradores de la compañía, modificar los proyectos y los permisos de los usuarios de su compañia. Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image13.png
   :align:   center
   :width: 80%


Consultar accesos
-----------------
Permite a los administradores de la compañía, consultar los accesos de los usuarios de su compañia. Se crea una tabla con la fecha, proyecto, actividad y usuario. Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image14.png
   :align:   center
   :width: 80%


Tokens
------
Permite a los usuarios consultar su nombre de usuario y su tokens.

.. figure::  images/02_interfaz/image15.png
   :align:   center
   :width: 80%


Log out
-------
Para cerrar sesión pulsamos en log out.