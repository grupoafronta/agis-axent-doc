# README #

Esto es la documentación de DJANGO

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## Traducciones 

### PARTE 1: DESDE PyCharm

#### Instalamos dependecias

```python
pip install sphinx-intl
```

#### Generamos el HTML

```python
make html
```

#### Extraer archivos pot
```python
make gettext
```

#### Añadir idiomas 

```python
sphinx-intl update -p build/gettext -l es -l en 
```
 Podemos encadenar los idiomas que deseemos agregando:

```python
-l xx
```

Esto genera source/locale/(idioma)/LC_MESSAGES/ y en su interior contiene los archivos .po

#### Traducir archivos .po
Traducir los archivos .po generados en la carpeta mencionada en el paso anterior

#### Cambiar la ruta de guardado del html
Para ello iremos a dos archivos y cambiaremos las siguientes lineas:
```python
# make.bat
set BUILDDIR=build
```
```python
# Makefile
BUILDDIR      = build
```

Por ejemplo si traducimos al inglés

```python
# make.bat
set BUILDDIR=build_en
```
```python
# Makefile
BUILDDIR      = build_en
```

Esto generará una carpeta llamada build_en cuando invoquemos el comando make


#### Generar html
Genera un html usando el idioma las traducciones que elijamos


    make -e SPHINXOPTS="-Dlanguage='en'" html


#### IMPORTANTE!!!
Por cada idioma nuevo debemos cambiar la ruta de guardado y cuando terminemos de generar los html de los diferentes idiomas reestablecer la ruta de guardado a build.


### PARTE 2: DESDE ReadTheDocs

Si está implementando un nuevo idioma, debe añadirlo a ReadTheDocs, en caso contrario no es necesario este paso.

#### Importar repositorio
En primer lugar debemos loguearnos en readthedoc con el usuario correspondiente. 
Una vez dentro pusaremos los botones:

Importar un proyecto > Importar manualmente

- En el apartado nombre pondremos agis-telco-doc-XX donde XX serán las siglas del idioma.


- En la dirección URL ponemos: https://bitbucket.org/grupoafronta/agis-telco-doc.git


- Tipo de repositorio: Git


- La rama la dejamos en blanco


- Marcamos la casilla de opciones avanzadas

Le damos a siguiente. En la pantalla que se muestra solo modificaremos el idioma.

Le damos a Terminar.

#### Modificar Proyecto agis-telco-doc-XX

Ahora desde el nuevo proyecto vamos a la pestaña visión general, donde pone versiones y aparece latest, le damos a editar.

Comprobamos que se encuentra activa y marcamos la casilla Hidden para ocultarla. Guardamos

#### Modificar Proyecto agis-telco-doc

Una vez hecho esto debemos cambiar al proyecto que se llama agis-telco-doc. Vamos a Administrador > Traducciones, seleccionamos el proyecto que hemos añadido y le damos a añadir.

Arriba aparecerá los idiomas disponibles

#### Comprobación en https://agis-telco-doc.readthedocs.io/es/latest/

Para cambiar entre los diferentes idiomas debemos hacer clic en la parte inferior izquierda, donde al pulsar se abre un desplegable donde elejimos el idioma.

A veces no aparece el nuevo idioma, simplemente hacemos un CNTRL + F5